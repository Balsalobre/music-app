import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(songs: any, search: any): any {
    if(search === undefined) return songs;
    return songs.filter(function(song){
    	return song.name.toLowerCase().includes(search.toLowerCase());
    })  
  }

}
