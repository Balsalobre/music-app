export class User{
	//De esta forma no necesitamos ni getter ni setter
	//Recomendable el uso de visibilidad
	constructor(		
			public _id: string, 
			public name: string,
			public surname: string,
			public email: string,
			public password: string,
			public role: string,
			public image: string
		){}
}