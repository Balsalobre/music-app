import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Song } from '../models/song';

@Injectable()
export class SongService{
	public url: string; //la url de nuestra api rest
											//inyectamos la dependencia por el constructor
											//podemos usar el objeto http y toda su librería de métodos
	constructor(private _http: Http){ 
		this.url = GLOBAL.url;
	}

	getAllSongs(token, albumId = null){
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});
		
		let options = new RequestOptions({headers:headers});
		if(albumId == null){
			return this._http.get(this.url+'songs', options)
							.map(res => res.json());
		}else{
			return this._http.get(this.url+'songs/'+albumId, options)
							.map(res => res.json());
		}
	}

	getSongs(token, albumId = null){
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		let options = new RequestOptions({headers: headers});
		
		if(!albumId == null){
			return this._http.get(this.url+'songs', options)
							.map(res => res.json());
		}else{
			return this._http.get(this.url+'songs/'+albumId, options)
							.map(res => res.json());
		}
	}

	getSong(token, id: string){
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		let options = new RequestOptions({headers: headers});
		return this._http.get(this.url+'song/'+id, options)
							.map(res => res.json());
	}

	addSong(token, song: Song){		//recibimos ese objeto en el método
		let params = JSON.stringify(song);		//lo convertimos en un string de JSON 
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		return this._http.post(this.url+'song', params, {headers: headers})
										.map(res => res.json()); //la respuesta que nos devuelve el api la convertimos en un objeto JSON
	}

	editSong(token, id:string, song: Song){		//recibimos ese objeto en el método
		let params = JSON.stringify(song);		//lo convertimos en un string de JSON 
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		return this._http.put(this.url+'song/'+id, params, {headers: headers})
										.map(res => res.json()); //la respuesta que nos devuelve el api la convertimos en un objeto JSON
	}

	deleteSong(token, id: string){
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		let options = new RequestOptions({headers: headers});
		return this._http.delete(this.url+'song/'+id, options)
							.map(res => res.json());
	}

}