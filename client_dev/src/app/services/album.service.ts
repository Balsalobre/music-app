import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Album } from '../models/album';

@Injectable()
export class AlbumService{
	public url: string; //la url de nuestra api rest
											//inyectamos la dependencia por el constructor
											//podemos usar el objeto http y toda su librería de métodos
	constructor(private _http: Http){ 
		this.url = GLOBAL.url;
	}

	getAlbums(token, artistId = null){
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		let options = new RequestOptions({headers:headers});
		if(artistId == null){
			return this._http.get(this.url+'albums', options)
										.map(res => res.json()); //para convertir lo que nos devuelve el api en un objeto JSON de JavaScript
		}else{
			return this._http.get(this.url+'albums/'+artistId, options)
										.map(res => res.json()); //para convertir lo que nos devuelve el api en un objeto JSON de JavaScript
		}
		
	}

	getAlbum(token, id: string){
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		let options = new RequestOptions({headers:headers});
		return this._http.get(this.url+'album/'+id, options)
										.map(res => res.json()); //para convertir lo que nos devuelve el api en un objeto JSON de JavaScript
	}

	addAlbum(token, album: Album){
		let params = JSON.stringify(album);
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		return this._http.post(this.url+'album', params, {headers: headers})
										.map(res => res.json()); //la respuesta que nos devuelve el api la convertimos en un objeto JSON
	}

	editAlbum(token, id:string, album: Album){
		let params = JSON.stringify(album);
		let headers = new Headers({
			'Content-Type':'application/json',
			'Authorization':token
		});

		return this._http.put(this.url+'album/'+id, params, {headers: headers})
										.map(res => res.json()); //la respuesta que nos devuelve el api la convertimos en un objeto JSON
	}

	deleteAlbum(token, id: string){
		let headers = new Headers({
			'Content-Type':'application/json', //Configuramos las cabeceras de autorización
			'Authorization':token
		});

		let options = new RequestOptions({headers:headers}); //Convertimos esas cabeceras en opciones, prar pasárselas a la petición get
		return this._http.delete(this.url+'album/'+id, options)
										.map(res => res.json()); //para convertir lo que nos devuelve el api en un objeto JSON de JavaScript
	}

}