import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { AlbumService } from '../services/album.service';
import { Album } from '../models/album';

@Component({
	selector: 'album-list',
	templateUrl: '../views/album-list.html',
	providers: [UserService, AlbumService]
})

export class AlbumListComponent implements OnInit{
	public titulo: string;
	public albums: Album[];
	public identity;
	public token;
	public url: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _albumService: AlbumService,
		private _userService: UserService
	){
		this.titulo = 'Albums';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
	}

	ngOnInit(){
		console.log('albums-list.component.ts cargado');

		//Conseguir el listado de albums
		this.getAlbums();
	}

	getAlbums(){
		
			this._albumService.getAlbums(this.token).subscribe(
				response => {
					if(!response.albums){
						this._router.navigate(['/']);	
					}else{
						this.albums = response.albums;
					}
				},
				error => {
					var errorMessage = <any>error;

	  			if(errorMessage != null){
	  				var body = JSON.parse(error._body);
	  				//this.alertMessage = body.message;

	  				console.log(error);
	  			}
				}
			);

		
	}

}
