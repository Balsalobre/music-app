import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FilterPipe } from '../filter.pipe';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { SongService } from '../services/song.service';
import { Song } from '../models/song';

@Component({
	selector: 'song-list',
	templateUrl: '../views/song-list.html',
	providers: [UserService, SongService]
})

export class SongListComponent implements OnInit{
	public titulo: string;
	public songs: Song[];
	public identity;
	public token;
	public url: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _songService: SongService,
		private _userService: UserService,
	){
		this.titulo = 'Busqueda por canción';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
	}

	ngOnInit(){
		console.log('song-list.component.ts cargado');

		//Conseguir el listado de canciones
		this.getSongs();
	}

	getSongs(){

		//Sacar las canciones 
			this._songService.getAllSongs(this.token).subscribe(
				response => {
					
					if(!response.songs){
						this._router.navigate(['/']);	
					}else{
						this.songs = response.songs;
					}
				},
				error => {
					var errorMessage = <any>error;

	  			if(errorMessage != null){
	  				var body = JSON.parse(error._body);
	  				//this.alertMessage = body.message;

	  				console.log(error);
	  			}
				}
			);
		
	}

	startPlayer(song){
		//AfterChecked entonces?

		let song_player = JSON.stringify(song);
		let file_path = this.url + 'get-song-file/' + song.file;
		let image_path = this.url + 'get-image-album/' + song.album.image;

		localStorage.setItem('sound_song', song_player);

		document.getElementById("mp3-source").setAttribute("src", file_path);
		(document.getElementById("player") as any).load(); //magia
		(document.getElementById("player") as any).play();

		document.getElementById('play-song-title').innerHTML = song.name;
		document.getElementById('play-song-artist').innerHTML = song.album.artist.name;
		document.getElementById('play-image-album').setAttribute('src', image_path);

	}

}
