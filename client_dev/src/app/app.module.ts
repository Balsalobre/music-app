import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; //two-way data binding
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders } from './app.routing';

//general
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home.component';

//user
import { UserEditComponent } from './components/user-edit.component';

//artist
import { ArtistListComponent } from './components/artist-list.component';
import { ArtistAddComponent } from './components/artist-add.component';
import { ArtistEditComponent } from './components/artist-edit.component';
import { ArtistDetailComponent } from './components/artist-detail.component';

//album
import { AlbumListComponent } from './components/album-list.component';
import { AlbumAddComponent } from './components/album-add.component';
import { AlbumEditComponent } from './components/album-edit.component';
import { AlbumDetailComponent } from './components/album-detail.component';

//song
import { SongListComponent } from './components/song-list.component';
import { SongAddComponent } from './components/song-add.component';
import { SongEditComponent } from './components/song-edit.component';
import { PlayerComponent } from './components/player.component';
import { FilterPipe } from './filter.pipe';

@NgModule({
  declarations: [ //cargamos componentes y directivas
    AppComponent,
    HomeComponent,
    UserEditComponent,
    ArtistListComponent,
    ArtistAddComponent,
    ArtistEditComponent,
    ArtistDetailComponent,
    AlbumListComponent,
    AlbumAddComponent,
    AlbumEditComponent,
    AlbumDetailComponent,
    SongListComponent,
    SongAddComponent,
    SongEditComponent,
    PlayerComponent,
    FilterPipe //cargamos las instancias de los objetos del componente
  ],
  imports: [  //cargamos módulos del framework
    BrowserModule,
    FormsModule,  //importante tener este módulo para poder hacer two-way data binding
    HttpModule,
    routing
  ],
  providers: [appRoutingProviders], //cargamos servicios
  bootstrap: [AppComponent] //punto principal donde carga la aplicación
})
export class AppModule { }
